
#include <stdio.h>
#include "include/base64.h"

int main( void ){
	char *code;
	base64_decode( &code, "Zg", 2 );
	printf( "%s\n", code );
	free( code );
	base64_decode( &code, "Zg=", 3 );
	printf( "%s\n", code );
	free( code );
	base64_decode( &code, "Zg==", 4 );
	printf( "%s\n", code );
	free( code );
}
