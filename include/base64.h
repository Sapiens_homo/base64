
#ifndef __BASE64_H__
#define __BASE64_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>

	size_t base64_encode( char **, const char *, size_t );
	size_t base64_decode( char **, const char *, size_t );

#ifdef __cplusplus
}
#endif

#endif
