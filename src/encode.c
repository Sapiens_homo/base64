
#include "../include/base64.h"

#include <stdint.h>

size_t base64_encode( char **dest, const char *src, size_t len ){
	const static char* const base64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	register size_t retlen = ( ( len + 2 ) / 3 )<<2;
	register size_t pad = ( 3 - ( len % 3 ) ) % 3;

	register uint32_t x;

	register size_t i;
	register size_t ii;
	register size_t iii;

	*dest = malloc( retlen + 1 );

	for ( i = 0, ii = 0; i < len; i += 3, ii += 4 ) {
		x = 0;
		for ( iii = 0; iii < 3; iii++ ){
			x <<= 8;
			if( i + iii < len ){
				x |= *( src + i + iii );
			}
		}
		iii = 4;
		while( iii-- ){
			*( *dest + ii + iii ) = *( base64 + ( x & 0x3F ) );
			x >>= 6;
		}
	}

	*( *dest + retlen ) = 0;
	while( pad-- ){
		*( *dest + retlen - 1 - pad ) = '=';
	}

	return retlen;
}
